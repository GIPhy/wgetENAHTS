#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  wgetENAHTS: downloading HTS files from ENA                                                                #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2021-2023 Institut Pasteur"                                                      #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#    Genome Informatics & Phylogenetics (GIPhy)                                            giphy.pasteur.fr  #
#    Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)            research.pasteur.fr/en/b/VTq  #
#   Julien Guglielmini                                                        julien.guglielmini@pasteur.fr  #
#    Bioinformatics and Biostatistics Hub     research.pasteur.fr/team/bioinformatics-and-biostatistics-hub  #
#    Dpt. Biologie Computationnelle                    research.pasteur.fr/department/computational-biology  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=4.2.231202acjg                                                                                     #
# + fixed bug to the modified file report format                                                             #
#                                                                                                            #
# VERSION=4.1.220307acjg                                                                                     #
# + fixed bugs with option -p                                                                                #
#                                                                                                            #
# VERSION=4.0.211129acjg                                                                                     #
# + for better download, adding automatic selection step between ftp and https protocols                     #
# + faster repository checking                                                                               #
# + downloading accessions sorted according to the decreasing total file size                                #
#                                                                                                            #
# VERSION=3.1.211118acjg                                                                                     #
# + fixed bug when downloading into an output directory                                                      #
#                                                                                                            #
# VERSION=3.0.211103acjg                                                                                     #
# + weh files completed with original md5 checksum(s)                                                        #
# + performs MD5-based instead of gzip-based file integrity assessment                                       #
#                                                                                                            #
# VERSION=2.0.210406acjg                                                                                     #
# + option -w removed                                                                                        #
# + one thread per file, instead of one thread per accession                                                 #
# + gzip file integrity assessment is also multi-threaded                                                    #
# + modified output                                                                                          #
#                                                                                                            #
# VERSION=1.0.210327acjg                                                                                     #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = INSTALLATION =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
#  Just give the execute permission to the script wgetENAHTS.sh with the following command line:             #
#                                                                                                            #
#   chmod +x wgetENAHTS.sh                                                                                   #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = FUNCTIONS    =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
# = echxit() =============================================================================================   #
#   prints in stderr the specified error message $1 and next exit 1                                          #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# = fb() =================================================================================================   #
#   prints the specified byte size $1 in rounded format                                                      #
#                                                                                                            #
fb() {
  if   [ $1 -gt 1073741824 ]; then echo "$(bc -l <<<"scale=2;$1/1073741824" | sed 's/^\./0\./') Gb" ;
  elif [ $1 -gt 1048576 ];    then echo "$(bc -l <<<"scale=2;$1/1048576"    | sed 's/^\./0\./') Mb" ;
  elif [ $1 -gt 1024 ];       then echo "$(bc -l <<<"scale=2;$1/1024"       | sed 's/^\./0\./') kb" ;
  else                             echo "$1 b" ; fi
}
typeset -fx fb ;
#                                                                                                            #
# = chrono() =============================================================================================   #
#   prints formatted elapsed time                                                                            #
#                                                                                                            #
chrono() {
  s=$SECONDS; printf "[%02d:%02d]" $(( $s / 60 )) $(( $s % 60 )) ;
}    
#                                                                                                            #
# = mandoc() =============================================================================================   #
#   prints the doc                                                                                           #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m wgetENAHTS v$VERSION              $COPYRIGHT\033[0m";
  cat <<EOF

 USAGE:  wgetENAHTS.sh  [[-o <dir>]  [-f <infile>]  [-t <nthreads>]  
                         [-p <protocol>]  [-r <rate>]  [-n]  [-h]]  [<accn> ...] 

 Downloads FASTQ files corresponding to the specified DRR/ERR/SRR accession(s)
 Files are downloaded from the ENA ftp repository ftp.sra.ebi.ac.uk/vol1/fastq

 OPTIONS:
  -o <dir>    output directory (default: .)
  -f <file>   to read accession(s) from the specified file (default: all the last 
              arguments)
  -t <int>    number of thread(s) (default: 2)
  -p <string> force the transfer protocol, either ftp or https (default: auto)
  -r <int>    maximum download rate per file, in kb per seconds  (default: entire
              available bandwidth)
  -n          no file download, only check (default: not set)
  -h          prints this help and exits

 EXAMPLES:
  + downloading the SE FASTQ file corresponding to accession DRR000003:
     wgetENAHTS.sh  DRR000003

  + downloading FASTQ files corresponding to accessions ERR000001 and ERR000004:
     wgetENAHTS.sh  ERR000001  ERR000004

  + assessing the repository existence for accessions SRR9870010-39:
     wgetENAHTS.sh  -n  SRR98700{10..39}

  + downloading FASTQ files (if any) corresponding to accessions SRR9870010-39:
     wgetENAHTS.sh  SRR98700{10..39}

  + same as above with (at most) 6 parallel downloads and saved outputs:
     wgetENAHTS.sh  -t 6  SRR98700{10..39}  > log.txt  2> err.txt

  + downloading the FASTQ files from accessions available in the file accn.txt:
     wgetENAHTS.sh  -f accn.txt

  + same as above with 9 parallel downloads and 500kb/sec download rate per file:
     wgetENAHTS.sh  -t 9  -r 500  -f accn.txt

EOF
}
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- wget -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  WGET_BIN=wget;
  [ ! $(command -v $WGET_BIN) ] && echoxit "no $WGET_BIN detected" ;
  WGET_STATIC_OPTIONS="--quiet --retry-connrefused --no-check-certificate";     
  WGET="$WGET_BIN $WGET_STATIC_OPTIONS";
#                                                                                                            #
# -- gzip -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GZIP_BIN=gzip;
  [ ! $(command -v $GZIP_BIN) ] && echoxit "no $GZIP_BIN detected" ;
  GZIP_STATIC_OPTIONS="--quiet";     
  GZIP="$GZIP_BIN $GZIP_STATIC_OPTIONS";
#                                                                                                            #
##############################################################################################################


##############################################################################################################
####                                                                                                      ####
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                          ####
####                                                                                                      ####
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

FILE_REPORT="https://www.ebi.ac.uk/ena/portal/api/filereport?download=true&result=read_run&accession=";
WGET_DWNL="$WGET --read-timeout=200 --continue --no-directories"; 
WGET_READ="$WGET --read-timeout=200 --output-document -";
WGET_TEST="$WGET --spider";

NTHREADS=2;
OUTDIR=".";
INFILE="_N.A_";
MAXRATE="NA";
PROTOCOL="auto";
DWNL=true;

while getopts t:o:f:r:p:nh option
do
  case $option in
  t)  NTHREADS=$OPTARG   ;;
  o)  OUTDIR="$OPTARG"   ;;
  f)  INFILE="$OPTARG"   ;;
  r)  MAXRATE=$OPTARG    ;;
  p)  PROTOCOL="$OPTARG" ;;
  n)  DWNL=false         ;;
  h)  mandoc ;  exit 0   ;;
  \?) mandoc ;  exit 1   ;;
  esac
done
shift "$(( $OPTIND - 1 ))"

if [ "$OUTDIR" != "." ]
then
  if [ -e $OUTDIR ]
  then
    [ ! -d $OUTDIR ] && echoxit "not a directory (option -o): $OUTDIR" ;
    [ ! -w $OUTDIR ] && echoxit "no write permission (option -o): $OUTDIR" ;
  else
    if mkdir $OUTDIR &>/dev/null
    then echo -e "$(chrono)\t\tcreating $OUTDIR\n" ;
    else echoxit "unable to create directory (option -o): $OUTDIR" ;
    fi
  fi
  WGET_DWNL="$WGET_DWNL --directory-prefix=$OUTDIR" ;
fi
if [ "$INFILE" != "_N.A_" ]
then
  [ ! -e $INFILE ] && echoxit "not found (option -f): $INFILE" ;
  [   -d $INFILE ] && echoxit "not a file (option -f): $INFILE" ;
  [ ! -s $INFILE ] && echoxit "empty file (option -f): $INFILE" ;
  [ ! -r $INFILE ] && echoxit "no read permission (option -f): $INFILE" ;
fi
if [ "$MAXRATE" != "NA" ]
then
  [[ $MAXRATE =~ ^[0-9]+$ ]] || echoxit "incorrect value (option -r): $MAXRATE" ;
   [ $MAXRATE -lt 1 ]        && MAXRATE=1;
  WGET_DWNL="$WGET_DWNL --limit-rate=$MAXRATE"k;
fi
if [ "$PROTOCOL" != "auto" ] && [ "$PROTOCOL" != "ftp" ] && [ "$PROTOCOL" != "https" ]
then
  echoxit "transfer protocol should be either ftp or https (option -p): $PROTOCOL" ;
fi
[[ $NTHREADS =~ ^[0-9]+$ ]]  || echoxit "incorrect value (option -t): $NTHREADS" ; 
 [ $NTHREADS -lt 1 ]         && NTHREADS=1;

wt=0; while [ $(( $wt * $wt )) -lt $NTHREADS ]; do let wt++ ; done
WAITIME=$wt;
[ $WAITIME -lt 1 ] && WAITIME=1;


##############################################################################################################
####                                                                                                      ####
#### CHECKING ACCESSIONS                                                                                  ####
####                                                                                                      ####
##############################################################################################################

ACCNLIST="$@";
NA=$#;
if [ -s $INFILE ]
then
  ACCNLIST="$(tr -d '\r' < $INFILE) $ACCNLIST" ;
  NA=$(( $NA + $(tr -d '\r' < $INFILE | wc -l) ));
fi
[ $NA -eq 0 ] && echoxit "no accession found" ;

if [ $NA -eq 1 ]; then echo -e "$(chrono)\t\t$NA specified accession" ;
else                   echo -e "$(chrono)\t\t$NA specified accessions" ; fi
echo ;


##############################################################################################################
####                                                                                                      ####
#### ASSESSING TRANSFER PROTOCOL                                                                          ####
####                                                                                                      ####
##############################################################################################################

URL="ftp.sra.ebi.ac.uk/vol1/fastq";
if [ "$PROTOCOL" == "auto" ]
then
  echo -n -e "$(chrono)\t\tassessing transfer protocol ." ;
  PROTOCOL="ftp";
  if $WGET_TEST "$PROTOCOL""://""$URL/DRR001/"
  then
    time_ftp=$SECONDS;
    for i in {2..5} ; do echo -n "." ; timeout 2 $WGET_TEST "$PROTOCOL""://""$URL/DRR00$i/" &>/dev/null ; rm -f wget-log ; done
    time_ftp=$(( $SECONDS - $time_ftp ));
  else
    rm -f wget-log ;
    echo -n "...." ;
    time_ftp=10;
  fi
  PROTOCOL="https";
  if $WGET_TEST "$PROTOCOL""://""$URL/DRR001/"
  then
    time_https=$SECONDS;
    for i in {2..5} ; do echo -n "." ; timeout 2 $WGET_TEST "$PROTOCOL""://""$URL/DRR00$i/" &>/dev/null ; rm -f wget-log ; done
    time_https=$(( $SECONDS - $time_https ));
  else
    rm -f wget-log ;
    echo -n "...." ;
    time_https=10;
  fi
  echo ". [ok]" ;
  if [ $time_ftp -lt $time_https ]
  then PROTOCOL="ftp";   echo -e "$(chrono)\t\tselected protocol: ftp ($time_ftp<$time_https)" ;
  else PROTOCOL="https"; echo -e "$(chrono)\t\tselected protocol: https ($time_https<$time_ftp)" ;
  fi
  echo ;
fi
FTPENA="$PROTOCOL""://""$URL" ;


##############################################################################################################
####                                                                                                      ####
#### CHECKING REPOSITORIES                                                                                ####
####                                                                                                      ####
##############################################################################################################

trap "echo;echo interrupting;rm -f $OUTDIR/\$a*.weh;exit 1;" SIGINT ;

C=0;
DL=0;
for ACCN in $ACCNLIST
do
  let C++ ;
  [ "$OUTDIR" == "." ] && OUTFQ=$ACCN*.fastq.gz || OUTFQ=$OUTDIR/$ACCN*.fastq.gz; 
  if $DWNL && ls $OUTFQ &>/dev/null
  then
    echo -e "[$C/$NA]\t\t\033[34mfile(s) already exist(s) for accession $ACCN\033[0m"    >&2 ;
    stat -c "%s %n" $OUTFQ | while read s n ; do echo -e "\t\t\033[90m$n\t[$(fb $s)]\033[0m" >&2 ; done 
    continue ;
  fi
  nc=${#ACCN};
  if   [ $nc -eq 9 ];  then URL="$FTPENA/${ACCN:0:6}/$ACCN/";
  elif [ $nc -eq 10 ]; then URL="$FTPENA/${ACCN:0:6}/00${ACCN:9:1}/$ACCN/";
  else                      URL="$FTPENA/${ACCN:0:6}/0${ACCN:9:2}/$ACCN/";
  fi
  if $WGET_TEST $URL || $WGET_TEST $URL
  then
    if $DWNL
    then
      echo -n -e "[$C/$NA]\t\tchecking repository for accession $ACCN ." ;
      sleep 0.1 &>/dev/null ;
      let DL++;
      echo -n "." ;
      # file report cols: 1         2           3         4       5         6       7             8               9             10      11        12        
      # f                 fastq_ftp fastq_bytes fastq_md5 bam_ftp bam_bytes bam_md5 submitted_ftp submitted_bytes submitted_md5 sra_ftp sra_bytes sra_md5 run_accession
      while true; do $WGET_READ "$FILE_REPORT$ACCN" 2>&1 && break; done | sed -n '$p' | cut -f1-3 | tr '\t' '\n' > $OUTDIR/$ACCN.weh ;
      echo -n "." ;
      if [ ! -s $OUTDIR/$ACCN.weh ]
      then
        while true; do $WGET_READ "$FILE_REPORT$ACCN" 2>&1 && break; done | sed -n '$p' | cut -f1-3 | tr '\t' '\n' > $OUTDIR/$ACCN.weh ;
        echo -n "." ;
      fi
      if [ ! -s $OUTDIR/$ACCN.weh ]
      then
	echo -e "\033[31m [fail]\033[0m" ;
	echo -e "[$C/$NA]\t\t\033[31mno information available for accession $ACCN\033[0m" >&2 ;
	rm -f $OUTDIR/$ACCN.weh ;
	let DL--;
	continue;
      fi
      sed -n 3p $OUTDIR/$ACCN.weh | tr ';' '\n'                  > $OUTDIR/$ACCN.md5.weh  ;
      sed -n 2p $OUTDIR/$ACCN.weh | tr ';' '\n'                  > $OUTDIR/$ACCN.size.weh ;
      sed -n 1p $OUTDIR/$ACCN.weh | tr ';' '\n' | sed 's@.*/@@g' > $OUTDIR/$ACCN.file.weh ;
      paste $OUTDIR/$ACCN.md5.weh $OUTDIR/$ACCN.size.weh $OUTDIR/$ACCN.file.weh | tr '\t' ' ' | sed 's/ /   /g' > $OUTDIR/$ACCN.weh ;
      rm $OUTDIR/$ACCN.md5.weh $OUTDIR/$ACCN.size.weh $OUTDIR/$ACCN.file.weh ;
      echo " [ok]" ;
    else
      echo -e "[$C/$NA]\t\t\033[32mexisting repository for accession $ACCN: $URL\033[0m"  >&2 ;
    fi	
  else
    echo -e "[$C/$NA]\t\t\033[31mnothing found for accession $ACCN\033[0m"                >&2 ;
  fi
  sleep 0.5 &>/dev/null ;
done 

echo ; 
if   [ $DL -eq 0 ]; then echo -e    "$(chrono)\t\texiting" ;      exit 0 ;
elif [ $DL -eq 1 ]; then echo -e -n "$(chrono)\t\t$DL valid accession; " ;
else                     echo -e -n "$(chrono)\t\t$DL valid accessions; " ; fi
N=0;
for ACCN in $ACCNLIST
do
  if [ ! -s $OUTDIR/$ACCN.weh ]; then continue ; fi
  nf=$(grep -c -F ".fastq.gz" $OUTDIR/$ACCN.weh);
  N=$(( $N + $nf ));
done
if [ $N -eq 1 ]; then echo "$N file to download" ;
else                  echo "$N files to download" ; fi
echo ;


##############################################################################################################
####                                                                                                      ####
#### SORTING ACCESSIONS                                                                                   ####
####                                                                                                      ####
##############################################################################################################

ACCNSORT="$(for ACCN in $ACCNLIST ; do [ -s $OUTDIR/$ACCN.weh ] && echo -e "$(sed 's/   /\t/g' $OUTDIR/$ACCN.weh | cut -f2 | paste -sd+ | bc -l)\t$ACCN" ; done | sort -gr | cut -f2 | tr '\n' ' ')";


##############################################################################################################
####                                                                                                      ####
#### DOWNLOADING FASTQ FILES                                                                              ####
####                                                                                                      ####
##############################################################################################################

C=0;
for ACCN in $ACCNSORT
do
  if [ ! -s $OUTDIR/$ACCN.weh ]; then continue ; fi
  nc=${#ACCN};
  if   [ $nc -eq 9 ];  then URL="$FTPENA/${ACCN:0:6}/$ACCN/";
  elif [ $nc -eq 10 ]; then URL="$FTPENA/${ACCN:0:6}/00${ACCN:9:1}/$ACCN/";
  else                      URL="$FTPENA/${ACCN:0:6}/0${ACCN:9:2}/$ACCN/";
  fi
  for FQGZ in $(sed "s/.*$ACCN/$ACCN/g" $OUTDIR/$ACCN.weh)
  do
    let C++ ;
    CMD1="echo -e \"[$C/$N]$'\t'$'\t'downloading$'\t'$FQGZ\" ; sleep $WAITIME";
    CMD2="while true ; do nice $WGET_DWNL $URL$FQGZ && break ; done";
    CMD3="md5sum -b $OUTDIR/$FQGZ > $OUTDIR/$FQGZ.weh";
    CMD4="s=\$(fb \$(stat -c %s $OUTDIR/$FQGZ))";
    CMD5="echo \"[$C/$N]$'\t'$'\t'completed$'\t'$FQGZ$'\t'[\$s]\"";
    echo "$CMD1 ; $CMD2 ; $CMD3 ; $CMD4 ; $CMD5" ;
    sleep $WAITIME ;
  done
done | xargs -P $NTHREADS -I CMD bash -c CMD ;

N=0;
for ACCN in $ACCNLIST
do
  if [ ! -s $OUTDIR/$ACCN.weh ]; then continue ; fi
  for FQGZ in $(sed "s/.*$ACCN/$ACCN/g" $OUTDIR/$ACCN.weh)
  do
    [ -e $OUTDIR/$FQGZ ] && let N++;
  done
done

echo ; 
if [ $N -eq 1 ]; then echo -e "$(chrono)\t\t$N downloaded file" ;
else                  echo -e "$(chrono)\t\t$N downloaded files" ; fi


##############################################################################################################
####                                                                                                      ####
#### CHECKING FASTQ FILES AND DOWNLOADING AGAIN (IF REQUIRED)                                             ####
####                                                                                                      ####
##############################################################################################################

C=0;
for ACCN in $ACCNSORT
do
  if [ ! -s $OUTDIR/$ACCN.weh ]; then continue ; fi
  nc=${#ACCN};
  if   [ $nc -eq 9 ];  then URL="$FTPENA/${ACCN:0:6}/$ACCN/";
  elif [ $nc -eq 10 ]; then URL="$FTPENA/${ACCN:0:6}/00${ACCN:9:1}/$ACCN/";
  else                      URL="$FTPENA/${ACCN:0:6}/0${ACCN:9:2}/$ACCN/";
  fi
  for FQGZ in $(sed "s/.*$ACCN/$ACCN/g" $OUTDIR/$ACCN.weh)
  do
    let C++ ;
    if [ ! -e $OUTDIR/$FQGZ ] || [ ! -s $OUTDIR/$FQGZ.weh ] || [ $(grep -c -F "$(sed 's/ .*//' $OUTDIR/$FQGZ.weh)" $OUTDIR/$ACCN.weh) -ne 1 ]
    then
      # rm -f $OUTDIR/$FQGZ ;
      echo -e "\033[31m[WARNING]\t\tproblem with file $FQGZ\033[0m" >&2 ;
      CMD1="echo \"[$C/$N]$'\t'$'\t'downloading$'\t'$FQGZ\" ; sleep $WAITIME";
      CMD2="while true ; do nice $WGET_DWNL $URL$FQGZ && break ; done";
      CMD3="md5sum -b $OUTDIR/$FQGZ > $OUTDIR/$FQGZ.weh";
      CMD4="s=\$(fb \$(stat -c %s $OUTDIR/$FQGZ))";
      CMD5="echo \"[$C/$N]$'\t'$'\t'completed$'\t'$FQGZ$'\t'[\$s]\"";
      echo "$CMD1 ; $CMD2 ; $CMD3 ; $CMD4 ; $CMD5" ;
      sleep $WAITIME ;
    fi
  done
done | xargs -P $NTHREADS -I CMD bash -c CMD ;

echo ; 

##############################################################################################################
####                                                                                                      ####
#### FINALIZING                                                                                           ####
####                                                                                                      ####
##############################################################################################################

C=0;
for ACCN in $ACCNLIST
do
  if [ ! -s $OUTDIR/$ACCN.weh ]; then continue ; fi
  nc=${#ACCN};
  if   [ $nc -eq 9 ];  then URL="$FTPENA/${ACCN:0:6}/$ACCN/";
  elif [ $nc -eq 10 ]; then URL="$FTPENA/${ACCN:0:6}/00${ACCN:9:1}/$ACCN/";
  else                      URL="$FTPENA/${ACCN:0:6}/0${ACCN:9:2}/$ACCN/";
  fi
  for FQGZ in $(sed "s/.*$ACCN/$ACCN/g" $OUTDIR/$ACCN.weh)
  do
    let C++ ;
    if   [ ! -s $OUTDIR/$FQGZ ]
    then
      echo -e "[$C/$N]\t\t\033[31m[FAIL] unable to download $URL/$FQGZ\033[0m"                          >&2 ;
      rm -f $OUTDIR/$ACCN.weh $OUTDIR/$FQGZ.weh ;
      continue;
    fi
    if [ ! -s $OUTDIR/$FQGZ.weh ]
    then
      md5sum -b $OUTDIR/$FQGZ > $OUTDIR/$FQGZ.weh ;
      if [ ! -s $OUTDIR/$FQGZ.weh ]
      then
        echo -e "[$C/$N]\t\t\033[31m[FAIL] unable to verify $OUTDIR/$FQGZ\033[0m"                       >&2 ;
	rm -f $OUTDIR/$ACCN.weh $OUTDIR/$FQGZ.weh ;
	continue ;
      fi
    fi
    if [ $(grep -c -F "$(sed 's/ .*//' $OUTDIR/$FQGZ.weh)" $OUTDIR/$ACCN.weh) -ne 1 ] 
    then 
      echo -e "[$C/$N]\t\t\033[31m[FAIL] incorrect MD5 checksum: $(sed 's/ .*//' $OUTDIR/$FQGZ.weh)\033[0m" >&2 ;
    else
      echo -e "[$C/$N]\t\t$(sed 's/ .*//' $OUTDIR/$FQGZ.weh)\t$FQGZ\t[$(fb $(stat -c %s $OUTDIR/$FQGZ))]" ;
    fi
    rm -f $OUTDIR/$FQGZ.weh ;
  done
  mv $OUTDIR/$ACCN.weh $OUTDIR/$ACCN.txt ;
done

echo ;
echo -e "$(chrono)\t\texiting" ;

exit 0 ;


